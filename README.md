# Sending Email Using Python
> This is for Nikko request

## Requirements
- `Python3` (FYI, I'am using 3.8.1) _[and for installing / downloading you can see here!](https://www.python.org/downloads/)_
- Configure your email on **Less Scure App Access** to be `ON`. Look image below:
![Scure Apps ON](/image-documentation/less-scure-apps-access-ON.jpg)
To process that click this link https://myaccount.google.com/lesssecureapps

## Library Used
- `smtplib` _[for the detail documentation you can see here!](https://docs.python.org/3/library/smtplib.html?highlight=smtplib#module-smtplib)_
- `email`
- `datetime`
- `os`

## Basic Info
For **main program** on `gmail.py`, and for **configuration** on `/conf/gmail_conf.py`

1. Knowing about `gmail.py` 
   
   Basicly on this file just having method for execute function `send_email`. To running it well we must sent parameter for function with `key` & `value` like below:
   ```
   send_email(receivers, subject, body, attachments=None):
   ...
   ```
   - `receivers` for **email target sender (MANDATORY)**. _It's can single target / multiple_. Below is sample:
     ```
     single: 'setiawanroni120@gmail.com'
     multiple: 'setiawanroni120@gmail.com, roni.nathsue07@gmail.com'
     ```
   - `subject` for **email subject (MANDATORY)**
     ```
     Example: 'Test Gmail-SMTP'
     ```
   - `body` for **body of your email (MANDATORY)**
     ```
     Example: 'This is body of testing gmail smtp using python'
     ```
   - `attachments` is **file attach (OPTIONAL)** to your email. _Make sure your file attch on directory that already you declare on **PATH_ATTACHMENT** in `/conf/gmail_conf.py`_. Sending this email using **array variable** and you can attach single / multiple file.
     ```
     Single: ['test-attach.txt']
     Multiple: ['test-attach.txt', 'DST.postman_collection.json']
     ```
     ![Path Attachment](/image-documentation/path-attachment.jpg)

2. Knowing about `/conf/gmail_conf.py`
   
   All configuration declare on this file. This guide you to assign configuration like below:
   ```
   <!-- Default for access gmail smtp -->
   SERVER = 'smtp.gmail.com'
   PORT = 587

   <!-- Change this based on your config -->
   SENDER = 'sroni@bajau.com'
   PASSWORD = 'XXXXXXXXXX'
   PATH_ATTACHMENT = 'C:/Users/Roni Setiawan/Desktop/'
   ```

## How To Running?
Basicly you can run into some way. They are:

1. Running directly after edit `gmail.py` to call function `send_email`.
   
   Here is step-by-step to do that.
   - Open `gmail.py` on your **IDE**, add this code 
     ```
     <!-- Case 1. Signle recivers without attachement -->
     now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
     send_email('roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now)

     <!-- Case 2. Multiple recivers without attachement -->
     now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
     send_email('roni.nathsue07@gmail.com, setiawanroni120@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now)

     <!-- Case 3. Signle recivers wit signle attachement -->
     now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
     send_email('roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now, ['test-attach.txt'])

     <!-- Case 4. Multiple recivers with multiple attachement -->
     now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
     send_email('roni.nathsue07@gmail.com, setiawanroni120@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now, ['test-attach.txt', 'DST.postman_collection.json'])
     ```
   - Open terminal / cmd, change into directory where `gmail.py` located
   - running this command
     ```
     $ python3 gmail.py
     ```
2. Running by import function `send_email` from `gmail.py`.
   
   Here step-by-step to do that.
   - Open terminal / cmd change into directory where `gmail.py` located
   - run this command
     ```
     python -c 'from gmail import send_email; send_email("roni.nathsue07@gmail.com, setiawanroni120@gmail.com", "Test Gmail-SMTP", "This is body of testing gmail smtp using python", ["test-attach.txt", 'DST.postman_collection.json"])'
     ```