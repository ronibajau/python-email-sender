import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase
from email import encoders
from datetime import datetime
from os.path import join as join_path
from conf.gmail_conf import SERVER, PORT, SENDER, PASSWORD, PATH_ATTACHMENT
   
def send_email(receivers, subject, body, attachments=None):  
  msg = MIMEMultipart() 
  
  msg['From'] = SENDER 
  msg['To'] = receivers
  msg['Subject'] = subject
  msg.attach(MIMEText(body, 'plain')) 
  
  if attachments != None:
    for fn in attachments or []:
      with open(join_path(PATH_ATTACHMENT, fn), 'rb') as f:
        attch = MIMEBase('application', 'octet-stream')
        attch.set_payload((f).read())
        encoders.encode_base64(attch)
        attch.add_header('Content-Disposition', 'attachment; filename="%s"' % fn)
      msg.attach(attch)

  s = smtplib.SMTP(SERVER, PORT)
  s.starttls()
  s.login(SENDER, PASSWORD)
  text = msg.as_string()
  s.sendmail(SENDER, receivers, text)
  s.quit()

if __name__ == '__main__':
  now = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
  # Case 1.
  # send_email('roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now)
  # Case 2.
  # send_email('setiawanroni120@gmail.com, roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now)
  # Case 3.
  # send_email('roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now, ['test-attach.txt'])
  # Case 4.
  # send_email('setiawanroni120@gmail.com, roni.nathsue07@gmail.com', 'Test Gmail-SMTP ' + now, 'This is body of testing gmail smtp using python ' + now, ['test-attach.txt', 'DST.postman_collection.json'])